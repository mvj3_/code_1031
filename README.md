 ASIHTTPRequest是iOS网络编程的利器。它封装了CFNetwork API，让很多网络通信功能的实现变得十分简单。它支持HTTP请求和基于REST的网络服务（包括GET、POST、 PUT、DELETE等）。具体来说，它支持的功能包括：

    1. 从服务器提交数据和获取数据，这一功能接口十分简单易用；

    2. 下载文件；

    3. 异步（Synchronous）或队列（Queue）请求网络服务；

    4. 支持Cookie。
